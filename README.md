# Front-End Developer Test

## Overview
This an example of using ReactJS to use the Starwars API (https://swapi.dev/) to look up characters and see what films they were in and what their species is. It uses dynamic searching to search by characters name. 

## Structure
The structure of app is as follows: 

The base has two folders API and Components. API folders holds our functions that deal with the SWAPI directly and the Components deal with all of the different parts of rendering the APP. Inside of the Components folder, you will find the APP folder which holds exculsively the APP.tsx (the inital render), the TOOLs folder which holds different tools to do data manipulation through out different parts of the render and the UI folder which deals with any component that actually renders data. 

+api
+components
++App
++Tools
++UI
+++Film
+++People
++++Person
+++Search
+++Species

## Tests
To run tests you can run `npm test` or `yarn test`

Tests are located in:
src > components > Tools > Tools.test.tsx
src > components > UI > Film > Film.test.tsx
src > components > UI > People > Person > Person.test.tsx
src > components > UI > Species > species.test.tsx

### Running the app
The app was created via `create-react-app` and has everything you'll need to build and test.

Run the app with `yarn start` or `npm run start`.


## Changes to the app 
This is v1 of the application. 
