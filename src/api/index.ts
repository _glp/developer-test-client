import UrlShort from '../components/Tools/urlShort'
export async function fetchJson<Response = any>(url: string, init?: RequestInit): Promise<Response> {

  //This searches the given URL to see if it has been given a full web address. If it is a full address this function
  //trims it down to get only the relevant part to the fetch
  if (url.includes('swapi.dev')){
    url = UrlShort(url);
  }

  //the calling of fetch
  const response = await fetch(
    `https://swapi.dev/api/${url}/`,
    {
      ...init ?? {},
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    //if there is an error in the response, throw an error
    if(!response.ok)
    {
      throw new Error('Something went wrong...');
    }

return response.json()
}

//this is a different function used for using a search variable
export async function fetchSearchJson<Response = any>(url: string, init?: RequestInit): Promise<Response> {

  const response = await fetch(
    `https://swapi.dev/api/people/?search=${url}`,
    {
      ...init ?? {},
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    //if there is an error throw an error
    if(!response.ok)
    {
      throw new Error('Something went wrong...');
    }

return response.json()
}
