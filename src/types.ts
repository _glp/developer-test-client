// TODO: define interface
export interface PersonType {
  name: string,
  films: string[],
  species: string[],
  url: string,
}

export interface FilmType {
  title: string
}

export interface SpeciesType {
  name: string
}
