import React from "react";
//a standard debounce custom hook to check to for delay before searching
//also has a clean up function
export function useDebounce(value:string, delay:number){
    const [debouncedValue, setDebouncedValue] = React.useState<string>("");

    React.useEffect(() =>{
        const handler = setTimeout(() => {
            setDebouncedValue(value);
            console.log(delay);
        }, delay);

        return()=>{
            clearTimeout(handler);
        };
    },
    [value, delay]
    );
    return debouncedValue;
}