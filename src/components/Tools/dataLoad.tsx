//this is a bit of a dynamic function that will take any object (from the JSON response of SWAPI) and dynamically
//respond with either the title (film) or the name (character or species). If we were using this to "load" more data
//we would add it here
const dataLoad = (objectData: any) => {
    const loadedData = [];

        if ("name" in objectData)
        {
            if(objectData.name != null)
            {
                loadedData.push({name: objectData.name});           
            }
            else{
                
            }
        }
        if (objectData.hasOwnProperty('title'))
        {
            loadedData.push({title: objectData.title});
        }

        if(objectData.name === undefined && objectData.title === null)
        {
                console.log ("OBJ NAME: " + objectData.name);
        }
    return loadedData;
}
export default dataLoad