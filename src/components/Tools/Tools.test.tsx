import UrlShort from "./urlShort";
import DataLoad from "./dataLoad";
import { FilmType, SpeciesType } from "../../types";

import { render, screen } from '@testing-library/react';

describe('Tool/urlShort', () => {
    test('should shorten a URL', () => {
      const url:string = "https://swapi.dev/api/people/1/";
      const shortenUrl = UrlShort(url);
  
      render(
         <div>{shortenUrl}</div>
      )
  
      screen.getByText("people/1")
    })
  });

  describe('Tool/dataLoad', () => {
    test('should dynamically output title or name', () => {
      const films:FilmType = {title: 'Return of the Jedi'}
      const species:SpeciesType = {name: 'Gungan'}

      const loadedFilms = DataLoad(films);
      const loadedSpecies = DataLoad(species);

      render(
         <div>
             <div>{loadedFilms[0].title}  </div>
             <div>{loadedSpecies[0].name} </div>
        </div>
      )
  
      screen.getByText('Return of the Jedi');
      screen.getAllByText('Gungan');
    })
  });
