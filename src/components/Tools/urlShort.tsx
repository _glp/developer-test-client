//a small function to take a string in and search through the string
//and pull out the information so that it can be used for the fetchJson function
const UrlShort = (url: string) => {
    
    let searchIndex:number = 0;
    let shortenURL:string = url;
    searchIndex = shortenURL.indexOf('/api'); 
    shortenURL = shortenURL.substring(searchIndex+5);
    shortenURL = shortenURL.slice(0,-1);

    return shortenURL;
}
export default UrlShort