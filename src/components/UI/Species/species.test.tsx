import { render, screen } from '@testing-library/react';
import { SpeciesType } from '../../../types';
import Species from './species'



describe('<Species />', () => {
  test('should render the person\'s species', () => {
    const species: SpeciesType = { name: 'Droid'}

    render(<Species species={species.name} />)

    screen.getByText(species.name)
  })
});