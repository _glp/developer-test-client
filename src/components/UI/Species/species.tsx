import {SpeciesType} from '../../../types'

//Species Type set as props
interface SpeciesProps {
  species: SpeciesType
}
//the species component to render what species is passed to it
const Species = ({ species }: SpeciesProps) => {
  console.log ("SPECIES: " + species)
  return (
        <div>
          <li className="ul-no-bullet">{species}</li>
        </div> 
      )
  }

export default Species