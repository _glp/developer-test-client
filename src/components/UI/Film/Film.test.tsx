import { render, screen } from '@testing-library/react';
import {FilmType} from '../../../types';
import Film from './Film'


describe('<Film />', () => {
  test('should render the Film\'s title', () => {
    const film: FilmType =  {title: 'A New Hope'}

    render(<Film film={film.title} />)

    screen.getByText(film.title)
  })
});