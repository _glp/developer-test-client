import {FilmType} from '../../../types'
//props typecasted for FilmProps type
interface FilmProps {
  film: FilmType
}

//component to render the film title given to it
const Film = ({ film }: FilmProps) => {
  //console.log ("FILM: " + film)
  return (
        <div>
          <li className="ul-no-bullet">{film}</li>
        </div> 
      )
  }

export default Film