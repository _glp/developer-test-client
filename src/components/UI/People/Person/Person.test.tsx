import { render, screen } from '@testing-library/react';
import { PersonType, FilmType} from '../../../../types';
import Person from './Person'
import Species from '../../Species/species'
import Films from '../../Film/Film'


describe('<Person />', () => {
  test('should render the person\'s name', () => {
    const person: PersonType = { name: 'Jek Tono Porkins'}

    render(<Person person={person} />)

    screen.getByText(person.name)
  })
});

describe('<Person />', () => {
  test('should render human when species is left blank', () => {
    const person: PersonType = { name: 'Luke Skywalker', species: [], films: ['Return of the Jedi']}

    render(
    <>
    <Person person={person} />
    </>
            )

    screen.getByText("Human")
  })
});
