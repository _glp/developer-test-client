import { PersonType, FilmType, SpeciesType} from '../../../../types';
import { fetchJson } from '../../../../api';
import dataLoad from '../../../Tools/dataLoad';
import Film from '../../Film/Film';
import Species from '../../Species/species';
import React, {useCallback} from 'react';
import UrlShort from '../../../Tools/urlShort';

//this is the Type casting of the person props to the PersonType
interface PersonProps {
  person: PersonType
}

function Person({ person }: PersonProps) {
  //Loading states for both films and species the two things we are rendering
  const [films, setFilms] = React.useState<FilmType[]>([]);
  const [species, setSpecies] = React.useState<SpeciesType[]>([]);
  const [imgUrl, setImgUrl] = React.useState<string>("");
  //adding an isLoading state
  const [isLoading, setLoading] = React.useState<boolean>(true);
  //this is a special variable to deal with how SWAPI treated the "human" species
  //as a blank array
  const loadHuman:any = {
    name: "Human"
  }

  //a function to fetch films related to the person passed in
  //we are using useCallback hook so that we do not create an
  //infinte loop in rendering (the useCallback checks to see 
  //if person.films is already running)
  const fetchFilms = useCallback( async() => {
     person.films.map(film => {
      fetchJson<{ results: FilmType[] }>(film).then(response => {
        const data = dataLoad(response);
        if(!films.includes(data[0].title))
        {
          setFilms((prevFilms) => {
            //we only grab title of the film we could change this to grab more
            //if we wanted to. 
            return prevFilms.concat(data[0].title);
            });
        }
        })
      });
  },[person.films]);

  const fetchUrl = () =>{
    console.log("hello");
    let cleanURL = UrlShort(person.url);

    console.log("clean pre:" + cleanURL);
    let cleanIndex = cleanURL.indexOf('people/'); 
    cleanURL = cleanURL.substring(cleanIndex+6);
    cleanURL = "https://starwars-visualguide.com/assets/img/characters"+cleanURL+".jpg";
    console.log("clean post:" + cleanURL);
    setImgUrl(cleanURL);
 }

  //a function to fetch Species related to the person passed in
  //we are again using the useCallback function related to 
  //the species to make sure we do not run into an infinite
  //loop while calling
  const fetchSpecies = useCallback( async() => {
    //this is a way of fixing the issue with SWAPI returning a 
    //blank species array when the species is human
   if(person.species.length === 0)
   {
      if(!species.includes(loadHuman.name))
      {
      setSpecies((prevSpecies) => {
        return prevSpecies.concat(loadHuman.name);
      })
     }
   }
   else{
    person.species.map(species => {
     fetchJson<{ results: SpeciesType[] }>(species).then(response => {
       const data = dataLoad(response);
       if(!species.includes(data[0].name))
       {
       setSpecies((prevSpecies) => {
         return prevSpecies.concat(data[0].name);
         });
        }
       })
     });
    }
 },[person.species, loadHuman.name])

//use effect to call the functions needed and do it again when 
//the functions are called again. We useCallback to prevent
//infinte calls
  React.useEffect(() => {
    setLoading(true);
    fetchFilms();
    fetchSpecies();
    fetchUrl();
    setLoading(false);
  },[fetchFilms, fetchSpecies])

 console.log(imgUrl);
  //Map the films to output per film component
  //map the species to output per species component
  return( 
    <div className="flip-card" >
      <div className="flip-card-inner">
        <div className="flip-card-front">
           <div className = "center">
           <img src={imgUrl} width="100" height="155" className="rounded"></img>
             {person.name}
            </div>           
        </div>
        <div className="flip-card-back">
            <h4>Films: </h4>
           <div>{(!isLoading) ? films.map(film => <Film key={film.title} film={film}/>):"Loading.."}</div>
           <h4>Species: </h4>
           <div>{(!isLoading) ? species.map(species=> <Species key={species.name} species={species} />):"Loading.."}</div>
        </div>
      </div>
    </div>
  )
}

export default Person
