import React from 'react'
import { fetchJson, fetchSearchJson } from '../../../api'
import { PersonType } from '../../../types'
import Person from './Person'
//setting the search state as part of the props passed to people
interface peopleProps{
  searchState: string,
}

function People({searchState}:peopleProps) {
  //setting states to be used. a people state for passing the persons
  //a Loading state to deal with when a component is loading
  //an Error state to show if there is an error in the fetch
  const [people, setPeople] = React.useState<PersonType[]>([])
  const [isLoading, setIsLoading] = React.useState<boolean>(true)
  const [isError, setErrorState] = React.useState<string>("")
 

  React.useEffect(() => {
    //set the loading state to true because we are begining a load
    setIsLoading(true)
    //if the searchState is passed is blank (or the first render)
    //send the people fetch to render
    if(searchState.trim().length === 0)
    {
      fetchJson<{ results: PersonType[] }>('people')
        .then(peopleResponse => setPeople(peopleResponse.results)).catch((error) =>
        {
          setIsLoading(false); //since we rendered an error we are no longer rendering
          setErrorState(error.message as string); //display the error messeage
        })
    }
    else
    {
      fetchSearchJson<{ results: PersonType[] }>(searchState)
      .then(peopleResponse => setPeople(peopleResponse.results)).catch((error) =>
      {
        setIsLoading(false); //since we rendered an error we are no longer rendering
        setErrorState(error.message as string); //display the error messeage
      })
    }
    setIsLoading(false); //since we made it here, we have rendered and are no longer loading
  }, [searchState]); //recall this useEffect everytime searchState is passed (because there has been a change)

  //if there is a error display the error (since its a return we won't render people)  
  if(isError)
  {
    return(
    <div>Error: {isError}</div>
    )
  }
 //if there is no error and we are no longer rendering, display the people by passing to the person component
  return (
    <div>
      {(isLoading) ? <p>Loading...</p> : people.map(person => <Person key={person.name} person={person} />)}
    </div>
  )
}

export default People
