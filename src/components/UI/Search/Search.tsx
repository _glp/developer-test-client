import { useDebounce } from "../../Tools/Debounce";
import React, { useEffect } from 'react'

const Search: React.FC<{ onSearch: (text: string)=> void }> = (props) => {
    //seeing states for searching and call the debounce function.
    const [searchTerm, setSearchTerm] = React.useState<string>("");
    const [isSearching, setIsSearching] = React.useState<boolean>(false);
    const debouncedSearchTerm = useDebounce(searchTerm, 1000);
   
    /*
    This is a way to do it via reference instead of just values
    //const searchValueInput = useRef<HTMLInputElement>(null);
    const submitHandler = (event: React.ChangeEvent) => {
        event.preventDefault();
        const enteredText = searchValueInput.current!.value;
        
    };*/

    useEffect(()=>{
        //if there is a search term that has been debounced then we can pass it on
        if(debouncedSearchTerm)
        {
            setIsSearching(true);
            props.onSearch(debouncedSearchTerm);
            setIsSearching(false);
        }
        //if there is not a search term, set it to blank so that we can re-render the base people 
        else{
            props.onSearch("");
        }

    },[debouncedSearchTerm, props])

    //rendering the input DOM 
    return(
        <div>
        <input 
            placeholder="Search for a character by name"
            onChange={e => setSearchTerm(e.target.value)}
            style={{ width:"300px" }}
        />

        {isSearching && <div>Searching...</div>}
        </div>
    )

}
export default Search