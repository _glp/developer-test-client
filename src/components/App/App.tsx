import People from '../UI/People';
import Search from '../UI/Search/Search';
import React from 'react';


function App() {
  //the state holder to hold and pass down the state of the search string
  const [searchState, setSearchState] = React.useState<string>("");

  //this loads the search state that was pulled up from the search 
  const onSearchHandler =(searchInput: string) =>{
    setSearchState(searchInput);
  }

  //this calls search and people to render 
  return (
    <div>
      <div className="center">
        <Search onSearch={onSearchHandler} />
      </div>
      <div className="container">
       <People searchState={searchState} /> 
      </div>
    </div>
  );
}

export default App;
